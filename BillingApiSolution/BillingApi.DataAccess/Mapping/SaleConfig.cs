﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using BillingApi.DataAccess.Entities;

namespace BillingApi.DataAccess.Mapping
{
    public class SaleConfig : IEntityTypeConfiguration<Sale>
    {
        public void Configure(EntityTypeBuilder<Sale> builder)
        {
            builder
                  .HasKey(c => c.SaleId);

            builder
                .Property(c => c.SaleId)
                .ValueGeneratedOnAdd();

            builder
                .Property(c => c.UniqueCode)
                .HasMaxLength(100)
                .IsRequired(true);
            builder
                .Property(c => c.HashCode)
                .HasMaxLength(1000)
                .IsRequired(true);
            builder
                .Property(c => c.Period)
                .IsRequired(true);
            builder
                .Property(c => c.OriginalSaleId)
                .IsRequired(false);
            builder
                .Property(c => c.OriginalSaleUniqueCode)
                .HasMaxLength(100)
                .IsRequired(false);
            builder
                .Property(c => c.CashBoxId)
                .IsRequired(true);
            builder
                .Property(c => c.BranchOfficeId)
                .IsRequired(true);
            builder
                .Property(c => c.PointOfSaleId)
                .IsRequired(true);
            builder
               .Property(c => c.BranchOfficeSinId)
               .IsRequired(true);
            builder
                .Property(c => c.PointOfSaleId)
                .IsRequired(true);
            builder
               .Property(c => c.ClientId)
               .HasMaxLength(25)
               .IsRequired(false);
            builder
                .Property(c => c.IdentityDocumentTypeId)
                .IsRequired(true);
            builder
               .Property(c => c.IdentityDocument)
               .HasMaxLength(22)
               .IsRequired(true);
            builder
                .Property(c => c.IdentityDocumentComplement)
                .HasMaxLength(5)
                .IsRequired(false);
            builder
               .Property(c => c.ClientFistName)
               .HasMaxLength(100)
               .IsRequired(true);
            builder
                .Property(c => c.ClientLastName)
                .HasMaxLength(100)
                .IsRequired(false);
            builder
               .Property(c => c.ClientEmail)
               .HasMaxLength(200)
               .IsRequired(false);
            builder
                .Property(c => c.ClientPhone)
                .HasMaxLength(20)
                .IsRequired(false);
            builder
               .Property(c => c.EconomicActivityTypeId)
               .IsRequired(true);
            builder
              .Property(c => c.EmissionDate)
              .HasColumnType(string.Format($"datetime"))
              .IsRequired(true);
            builder
              .Property(c => c.TotalDiscountPercent)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);
            builder
              .Property(c => c.TotalDiscountAmount)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);
            builder
              .Property(c => c.SubTotalAmount)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);
            builder
              .Property(c => c.TotalAmount)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);
            builder
              .Property(c => c.DonationAmount)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);
            builder
              .Property(c => c.TaxAmount)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);
            builder
              .Property(c => c.TaxPercent)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);
            builder
              .Property(c => c.TotalTaxedAmount)
              .HasColumnType("decimal(14, 2)")
              .IsRequired(true);
            builder
              .Property(c => c.EmissionTypeId)
              .IsRequired(true);

            builder
              .Property(c => c.CashierId)
              .HasMaxLength(20)
              .IsRequired(true);
            builder
              .Property(c => c.CashierUser)
               .HasMaxLength(50)
              .IsRequired(true);
            builder
              .Property(c => c.SectorDocumentId)
              .IsRequired(true);
            builder
              .Property(c => c.TurnId)
              .HasMaxLength(100)
              .IsRequired(true);
            builder
              .Property(c => c.ContingencyDate)
              .HasColumnType(string.Format($"datetime"))
              .IsRequired(false);
            builder
              .Property(c => c.KeyOrigen)
              .HasMaxLength(50)
              .IsRequired(true);
            builder
              .Property(c => c.ExtraInfo)
              .IsRequired(false);
            builder
             .Property(c => c.PostVoidDate)
             .HasColumnType(string.Format($"datetime"))
             .IsRequired(false);
            builder
             .Property(c => c.PostVoidMotiveId)
             .IsRequired(false);
            builder
             .Property(c => c.PostVoidOtherMotive)
             .HasMaxLength(400)
             .IsRequired(false);
            builder
             .Property(c => c.GenerateInvoice)
             .IsRequired(true);
            builder
             .Property(c => c.InvoiceTypeId)
             .IsRequired(true);
            builder
              .Property(c => c.State)
              .IsRequired(true);
            builder
              .Property(c => c.CreateDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));
            builder
              .Property(c => c.EditDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));

            builder
                .HasOne(c => c.OriginalSale)
                .WithMany(c => c.CreDeSale)
                .HasForeignKey(c => c.OriginalSaleId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.EconomicActivityType)
                .WithMany(c => c.Sales)
                .HasForeignKey(c => c.EconomicActivityTypeId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.FiscalDocumentType)
                .WithMany(c => c.Sales)
                .HasForeignKey(c => c.FiscalDocumentTypeId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.SaleState)
                .WithMany(c => c.Sales)
                .HasForeignKey(c => c.State)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.IdentityDocumentType)
                .WithMany(c => c.Sales)
                .HasForeignKey(c => c.IdentityDocumentTypeId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.FiscalDocumentCreDe)
                .WithOne(c => c.Sale)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.FiscalDocument)
                .WithOne(c => c.Sale)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.FiscalDocumentCreDe)
                .WithOne(c => c.Sale)
                .OnDelete(DeleteBehavior.NoAction);



        }
    }
}
