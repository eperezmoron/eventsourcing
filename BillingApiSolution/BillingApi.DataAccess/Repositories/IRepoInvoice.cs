﻿using BillingApi.DataAccess.Entities;
using BillingApi.Dto.DataTransfer;
using BillingApi.Dto.Param;
using Generic.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Repositories
{
    public interface IRepoInvoice : IRepoBase
    {
        DosageDto GetEnableDosage(GetEnableDosageParam param);
        DosageDto GetDosageById(long autorizationNumber);
        void IncInvoiceDosageNumber(IncInvoiceDosageNumberParam param);
        string GetEconomicActivityLeyend(short economicActivityId);
    }
}
