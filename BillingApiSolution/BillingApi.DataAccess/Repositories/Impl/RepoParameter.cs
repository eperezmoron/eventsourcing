﻿using BillingApi.DataAccess.UnitOfWork;
using Generic.Data.Repositories.Impl;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Repositories.Impl
{
    public class RepoParameter : RepoBase<IBillingUnitOfWork>, IRepoParameter
    {
        public static string KeyInvoiceType = "InvoiceType";
        public static string KeyCompanyAddress = "CompanyAddress";
        public static string KeyCompanyLeyend = "CompanyLeyend";
        public static string KeyCompanyNit = "CompanyNit";
        public static string KeyCompanyPhone = "CompanyPhone";
        public static string KeyQrLeyend = "QrLeyend";

        private IConfiguration _configuration;

        public RepoParameter(
            IConfiguration configuration,
            IBillingUnitOfWork unitOfWork) : base(unitOfWork)
        {
            this._configuration = configuration;
        }

        public string GetCompanyAddress()
        {
            return this._configuration.GetValue<string>(KeyCompanyAddress);
        }

        public string GetCompanyLeyend()
        {
            return this._configuration.GetValue<string>(KeyCompanyLeyend);
        }

        public string GetCompanyNit()
        {
            return this._configuration.GetValue<string>(KeyCompanyNit);
        }

        public string GetCompanyPhone()
        {
            return this._configuration.GetValue<string>(KeyCompanyPhone);
        }

        public short GetInvoiceType()
        {
            return this._configuration.GetValue<short>(KeyInvoiceType);
        }

        public string GetQrLeyend()
        {
            return this._configuration.GetValue<string>(KeyQrLeyend);
        }
    }
}
