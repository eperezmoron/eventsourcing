﻿using BillingApi.DataAccess.UnitOfWork;
using Generic.Data.Repositories.Impl;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Repositories.Impl
{
    public partial class RepoBilling : RepoBase<IBillingUnitOfWork>, IRepoBilling
    {
        public RepoBilling(IBillingUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public string GetFiscalDocumentTitle(short id)
        {
            return (from t in UnitOfWork.FiscalDocumentType
                    where t.FiscalDocumentTypeId == id
                    select t.Title).FirstOrDefault();
        }
    }
}
