﻿using BillingApi.DataAccess.Entities;
using BillingApi.DataAccess.UnitOfWork;
using BillingApi.Dto.DataTransfer;
using Generic.Data.Repositories.Impl;
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace BillingApi.DataAccess.Repositories.Impl
{
    public partial class RepoSale : RepoBase<IBillingUnitOfWork>, IRepoSale
    {
        public RepoSale(IBillingUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public SaleDto GetById(long param)
        {
            return (from sale in UnitOfWork.Sale
                    .Include(x => x.SaleDetails.Select(y => y.SaleLineDiscounts))
                    .Include(x => x.SaleDiscounts)
                    .Include(x => x.SalePayments)
                    .Include(x => x.SaleState)
                    where param == sale.SaleId
                    select new SaleDto()
                    {
                        CashierUser = sale.CashierUser,
                        BranchOfficeId = sale.BranchOfficeId,
                        BranchOfficeSinId = sale.BranchOfficeSinId,
                        CashBoxId = sale.CashBoxId,
                        CashierId = sale.CashierId,
                        ClientEmail = sale.ClientEmail,
                        ClientFistName = sale.ClientFistName,
                        ClientId = sale.ClientId,
                        ClientLastName = sale.ClientLastName,
                        ClientPhone = sale.ClientPhone,
                        PostVoidOtherMotive = sale.PostVoidOtherMotive,
                        ContingencyDate = sale.ContingencyDate,
                        CreateDate = sale.CreateDate,
                        DonationAmount = sale.DonationAmount,
                        EconomicActivityTypeId = sale.EconomicActivityTypeId,
                        EditDate = sale.EditDate,
                        EmissionDate = sale.EmissionDate,
                        EmissionTypeId = sale.EmissionTypeId,
                        ExtraInfo = sale.ExtraInfo,
                        FiscalDocumentTypeId = sale.FiscalDocumentTypeId,
                        HashCode = sale.HashCode,
                        InvoiceTypeId = sale.InvoiceTypeId,
                        KeyOrigen = sale.KeyOrigen,
                        IdentityDocument = sale.IdentityDocument,
                        OriginalSaleId = sale.OriginalSaleId,
                        OriginalSaleUniqueCode = sale.OriginalSaleUniqueCode,
                        Period = sale.Period,
                        GenerateInvoice = sale.GenerateInvoice,
                        PointOfSaleId = sale.PointOfSaleId,
                        PostVoidDate = sale.PostVoidDate,
                        PostVoidMotiveId = sale.PostVoidMotiveId,
                        IdentityDocumentTypeId = sale.IdentityDocumentTypeId,
                        SaleId = sale.SaleId,
                        State = sale.State,
                        IdentityDocumentComplement = sale.IdentityDocumentComplement,
                        SectorDocumentId = sale.SectorDocumentId,
                        TaxAmount = sale.TaxAmount,
                        TaxPercent = sale.TaxPercent,
                        TotalAmount = sale.TotalAmount,
                        TotalDiscountAmount = sale.TotalDiscountAmount,
                        TotalDiscountPercent = sale.TotalDiscountPercent,
                        TurnId = sale.TurnId,
                        UniqueCode = sale.UniqueCode,
                        TotalTaxedAmount = sale.TotalTaxedAmount,
                        SubTotalAmount = sale.SubTotalAmount,
                        SalePayment = sale.SalePayments.Select(p => new SalePaymentDto()
                        {
                            Amount = p.Amount,
                            AuthCode = p.AuthCode,
                            BankCode = p.BankCode,
                            BankName = p.BankName,
                            CardNumber = p.CardNumber,
                            Change = p.Change,
                            ClientCode = p.ClientCode,
                            ConfirmationCode = p.ConfirmationCode,
                            ExtraInfo = p.ExtraInfo,
                            PaymentType = p.PaymentType,
                            SaleId = p.SaleId,
                            SalePaymentId = p.SalePaymentId
                        }).ToList(),
                        FiscalDocument = sale.FiscalDocument == null ? null :
                        new FiscalDocumentDto()
                        {
                            SaleId = sale.FiscalDocument.SaleId,
                            AutorizationNumber = sale.FiscalDocument.AutorizationNumber,
                            BranchOfficeLeyend = sale.FiscalDocument.BranchOfficeLeyend,
                            CompanyAddress = sale.FiscalDocument.CompanyAddress,
                            CompanyLeyend = sale.FiscalDocument.CompanyLeyend,
                            CompanyNit = sale.FiscalDocument.CompanyNit,
                            CompanyPhone = sale.FiscalDocument.CompanyPhone,
                            ControlCode = sale.FiscalDocument.ControlCode,
                            Cuf = sale.FiscalDocument.Cuf,
                            Cufd = sale.FiscalDocument.Cufd,
                            Cuis = sale.FiscalDocument.Cuis,
                            DeadlineEmissionDate = sale.FiscalDocument.DeadlineEmissionDate,
                            DocumentFiscalTitle = sale.FiscalDocument.DocumentFiscalTitle,
                            DosageKey = sale.FiscalDocument.DosageKey,
                            EconomicActivityId = sale.FiscalDocument.EconomicActivityId,
                            EconomicActivityLeyend = sale.FiscalDocument.EconomicActivityLeyend,
                            EmissionDate = sale.FiscalDocument.EmissionDate,
                            FirstClientName = sale.FiscalDocument.FirstClientName,
                            FiscalDocumentLeyend = sale.FiscalDocument.FiscalDocumentLeyend,
                            FiscalDocumentTypeId = sale.FiscalDocument.FiscalDocumentTypeId,
                            IdentityComplement = sale.FiscalDocument.IdentityComplement,
                            IdentityDocument = sale.FiscalDocument.IdentityDocument,
                            IdentityDocumentTypeId = sale.FiscalDocument.IdentityDocumentTypeId,
                            InvoiceLeyend = sale.FiscalDocument.InvoiceLeyend,
                            InvoiceNumber = sale.FiscalDocument.InvoiceNumber,
                            InvoiceStateLeyend = sale.FiscalDocument.InvoiceStateLeyend,
                            LastClientName = sale.FiscalDocument.LastClientName,
                            PostVoidDate = sale.FiscalDocument.PostVoidDate,
                            PostVoidMotiveId = sale.FiscalDocument.PostVoidMotiveId,
                            QrBuffer = sale.FiscalDocument.QrBuffer,
                            QrLeyend = sale.FiscalDocument.QrLeyend,
                            State = sale.FiscalDocument.State,
                        },
                        FiscalDocumentCreDe = sale.FiscalDocumentCreDe == null ? null :
                        new FiscalDocumentCreDeDto()
                        {
                            OriginalAutorizationNumber = sale.FiscalDocumentCreDe.OriginalAutorizationNumber,
                            OriginalBranchOfficeId = sale.FiscalDocumentCreDe.OriginalBranchOfficeId,
                            OriginalCashboxId = sale.FiscalDocumentCreDe.OriginalCashboxId,
                            OriginalControlCode = sale.FiscalDocumentCreDe.OriginalControlCode,
                            OriginalCuf = sale.FiscalDocumentCreDe.OriginalCuf,
                            OriginalCufd = sale.FiscalDocumentCreDe.OriginalCufd,
                            OriginalCuis = sale.FiscalDocumentCreDe.OriginalCuis,
                            OriginalDosageKey = sale.FiscalDocumentCreDe.OriginalDosageKey,
                            OriginalEmissionDate = sale.FiscalDocumentCreDe.OriginalEmissionDate,
                            OriginalInvoiceNumber = sale.FiscalDocumentCreDe.OriginalInvoiceNumber,
                            OriginalPointOfSaleId = sale.FiscalDocumentCreDe.OriginalPointOfSaleId,
                            OriginalTotalAmount = sale.FiscalDocumentCreDe.OriginalTotalAmount,
                            OriginalTotalTaxedAmount = sale.FiscalDocumentCreDe.OriginalTotalTaxedAmount,
                            OriginalUniqueCode = sale.FiscalDocumentCreDe.OriginalUniqueCode,
                            SaleId = sale.FiscalDocumentCreDe.SaleId
                        },
                        SaleDetail = sale.SaleDetails.Select(x => new SaleDetailDto()
                        {
                            SaleId = x.SaleId,
                            Comments = x.Comments,
                            ExtraInfo = x.ExtraInfo,
                            ItemDescript = x.ItemDescript,
                            ItemEconomicActivityId = x.ItemEconomicActivityId,
                            ItemId = x.ItemId,
                            ItemIdSin = x.ItemIdSin,
                            ItemMeasurementUnit = x.ItemMeasurementUnit,
                            ItemMeasurementUnitSin = x.ItemMeasurementUnitSin,
                            ItemUnitPrice = x.ItemUnitPrice,
                            LineId = x.LineId,
                            Quantity = x.Quantity,
                            ReturnLineId = x.ReturnLineId,
                            SaleDetailId = x.SaleDetailId,
                            SaleDetailTypeId = x.SaleDetailTypeId,
                            SubTotalAmount = x.SubTotalAmount,
                            TaxAmount = x.TaxAmount,
                            TaxPercent = x.TaxPercent,
                            TotalAmount = x.TotalAmount,
                            TotalDiscount = x.TotalDiscount,
                            TotalDiscountPercent = x.TotalDiscountPercent,
                            TotalTaxedAmount = x.TotalTaxedAmount,
                            SaleLineDiscount = x.SaleLineDiscounts.Select(y => new SaleLineDiscountDto()
                            {
                                Amount = y.Amount,
                                AuthName = y.AuthName,
                                AuthUser = y.AuthUser,
                                Descript = y.Descript,
                                DiscountId = y.DiscountId,
                                DiscountType = y.DiscountType,
                                ExtraInfo = y.ExtraInfo,
                                Percent = y.Percent,
                                SaleDetailId = y.SaleDetailId,
                                SaleLineDiscountId = y.SaleLineDiscountId,
                            }).ToList(),
                        }).ToList(),
                        SaleDiscount = sale.SaleDiscounts.Select(x => new SaleDiscountDto()
                        {
                            Amount = x.Amount,
                            AuthName = x.AuthName,
                            AuthUser = x.AuthUser,
                            Descript = x.Descript,
                            DiscountId = x.DiscountId,
                            DiscountType = x.DiscountType,
                            ExtraInfo = x.ExtraInfo,
                            Percent = x.Percent,
                            SaleDiscountId = x.SaleDiscountId,
                            SaleId = x.SaleId
                        }).ToList()
                    }).FirstOrDefault();
        }

        public Sale Store(SaleDto param)
        {
            Sale sale = new Sale()
            {
                CashierUser = param.CashierUser,
                BranchOfficeId = param.BranchOfficeId,
                BranchOfficeSinId = param.BranchOfficeSinId,
                CashBoxId = param.CashBoxId,
                CashierId = param.CashierId,
                ClientEmail = param.ClientEmail,
                ClientFistName = param.ClientFistName,
                ClientId = param.ClientId,
                ClientLastName = param.ClientLastName,
                ClientPhone = param.ClientPhone,
                PostVoidOtherMotive = param.PostVoidOtherMotive,
                ContingencyDate = param.ContingencyDate,
                CreateDate = param.CreateDate,
                DonationAmount = param.DonationAmount,
                EconomicActivityTypeId = param.EconomicActivityTypeId,
                EditDate = param.EditDate,
                EmissionDate = param.EmissionDate,
                EmissionTypeId = param.EmissionTypeId,
                ExtraInfo = param.ExtraInfo,
                FiscalDocumentTypeId = param.FiscalDocumentTypeId,
                HashCode = param.HashCode,
                InvoiceTypeId = param.InvoiceTypeId,
                KeyOrigen = param.KeyOrigen,
                IdentityDocument = param.IdentityDocument,
                OriginalSaleId = param.OriginalSaleId,
                OriginalSaleUniqueCode = param.OriginalSaleUniqueCode,
                Period = param.Period,
                GenerateInvoice = param.GenerateInvoice,
                PointOfSaleId = param.PointOfSaleId,
                PostVoidDate = param.PostVoidDate,
                PostVoidMotiveId = param.PostVoidMotiveId,
                IdentityDocumentTypeId = param.IdentityDocumentTypeId,
                SaleId = param.SaleId,
                State = param.State,
                IdentityDocumentComplement = param.IdentityDocumentComplement,
                SectorDocumentId = param.SectorDocumentId,
                TaxAmount = param.TaxAmount,
                TaxPercent = param.TaxPercent,
                TotalAmount = param.TotalAmount,
                TotalDiscountAmount = param.TotalDiscountAmount,
                TotalDiscountPercent = param.TotalDiscountPercent,
                TurnId = param.TurnId,
                UniqueCode = param.UniqueCode,
                TotalTaxedAmount = param.TotalTaxedAmount,
                SubTotalAmount = param.SubTotalAmount,
                SalePayments = param.SalePayment.Select(p => new SalePayment()
                {
                    Amount = p.Amount,
                    AuthCode = p.AuthCode,
                    BankCode = p.BankCode,
                    BankName = p.BankName,
                    CardNumber = p.CardNumber,
                    Change = p.Change,
                    ClientCode = p.ClientCode,
                    ConfirmationCode = p.ConfirmationCode,
                    ExtraInfo = p.ExtraInfo,
                    PaymentType = p.PaymentType,
                    SaleId = p.SaleId,
                    SalePaymentId = p.SalePaymentId
                }).ToList(),
                FiscalDocument = param.FiscalDocument == null ? null :
                        new FiscalDocument()
                        {
                            SaleId = param.FiscalDocument.SaleId,
                            AutorizationNumber = param.FiscalDocument.AutorizationNumber,
                            BranchOfficeLeyend = param.FiscalDocument.BranchOfficeLeyend,
                            CompanyAddress = param.FiscalDocument.CompanyAddress,
                            CompanyLeyend = param.FiscalDocument.CompanyLeyend,
                            CompanyNit = param.FiscalDocument.CompanyNit,
                            CompanyPhone = param.FiscalDocument.CompanyPhone,
                            ControlCode = param.FiscalDocument.ControlCode,
                            Cuf = param.FiscalDocument.Cuf,
                            Cufd = param.FiscalDocument.Cufd,
                            Cuis = param.FiscalDocument.Cuis,
                            DeadlineEmissionDate = param.FiscalDocument.DeadlineEmissionDate,
                            DocumentFiscalTitle = param.FiscalDocument.DocumentFiscalTitle,
                            DosageKey = param.FiscalDocument.DosageKey,
                            EconomicActivityId = param.FiscalDocument.EconomicActivityId,
                            EconomicActivityLeyend = param.FiscalDocument.EconomicActivityLeyend,
                            EmissionDate = param.FiscalDocument.EmissionDate,
                            FirstClientName = param.FiscalDocument.FirstClientName,
                            FiscalDocumentLeyend = param.FiscalDocument.FiscalDocumentLeyend,
                            FiscalDocumentTypeId = param.FiscalDocument.FiscalDocumentTypeId,
                            IdentityComplement = param.FiscalDocument.IdentityComplement,
                            IdentityDocument = param.FiscalDocument.IdentityDocument,
                            IdentityDocumentTypeId = param.FiscalDocument.IdentityDocumentTypeId,
                            InvoiceLeyend = param.FiscalDocument.InvoiceLeyend,
                            InvoiceNumber = param.FiscalDocument.InvoiceNumber,
                            InvoiceStateLeyend = param.FiscalDocument.InvoiceStateLeyend,
                            LastClientName = param.FiscalDocument.LastClientName,
                            PostVoidDate = param.FiscalDocument.PostVoidDate,
                            PostVoidMotiveId = param.FiscalDocument.PostVoidMotiveId,
                            QrBuffer = param.FiscalDocument.QrBuffer,
                            QrLeyend = param.FiscalDocument.QrLeyend,
                            State = param.FiscalDocument.State,
                        },
                FiscalDocumentCreDe = param.FiscalDocumentCreDe == null ? null :
                        new FiscalDocumentCreDe()
                        {
                            OriginalAutorizationNumber = param.FiscalDocumentCreDe.OriginalAutorizationNumber,
                            OriginalBranchOfficeId = param.FiscalDocumentCreDe.OriginalBranchOfficeId,
                            OriginalCashboxId = param.FiscalDocumentCreDe.OriginalCashboxId,
                            OriginalControlCode = param.FiscalDocumentCreDe.OriginalControlCode,
                            OriginalCuf = param.FiscalDocumentCreDe.OriginalCuf,
                            OriginalCufd = param.FiscalDocumentCreDe.OriginalCufd,
                            OriginalCuis = param.FiscalDocumentCreDe.OriginalCuis,
                            OriginalDosageKey = param.FiscalDocumentCreDe.OriginalDosageKey,
                            OriginalEmissionDate = param.FiscalDocumentCreDe.OriginalEmissionDate,
                            OriginalInvoiceNumber = param.FiscalDocumentCreDe.OriginalInvoiceNumber,
                            OriginalPointOfSaleId = param.FiscalDocumentCreDe.OriginalPointOfSaleId,
                            OriginalTotalAmount = param.FiscalDocumentCreDe.OriginalTotalAmount,
                            OriginalTotalTaxedAmount = param.FiscalDocumentCreDe.OriginalTotalTaxedAmount,
                            OriginalUniqueCode = param.FiscalDocumentCreDe.OriginalUniqueCode,
                            SaleId = param.FiscalDocumentCreDe.SaleId
                        },
                SaleDetails = param.SaleDetail.Select(x => new SaleDetail()
                {
                    SaleId = x.SaleId,
                    Comments = x.Comments,
                    ExtraInfo = x.ExtraInfo,
                    ItemDescript = x.ItemDescript,
                    ItemEconomicActivityId = x.ItemEconomicActivityId,
                    ItemId = x.ItemId,
                    ItemIdSin = x.ItemIdSin,
                    ItemMeasurementUnit = x.ItemMeasurementUnit,
                    ItemMeasurementUnitSin = x.ItemMeasurementUnitSin,
                    ItemUnitPrice = x.ItemUnitPrice,
                    LineId = x.LineId,
                    Quantity = x.Quantity,
                    ReturnLineId = x.ReturnLineId,
                    SaleDetailId = x.SaleDetailId,
                    SaleDetailTypeId = x.SaleDetailTypeId,
                    SubTotalAmount = x.SubTotalAmount,
                    TaxAmount = x.TaxAmount,
                    TaxPercent = x.TaxPercent,
                    TotalAmount = x.TotalAmount,
                    TotalDiscount = x.TotalDiscount,
                    TotalDiscountPercent = x.TotalDiscountPercent,
                    TotalTaxedAmount = x.TotalTaxedAmount,
                    SaleLineDiscounts = x.SaleLineDiscount.Select(y => new SaleLineDiscount()
                    {
                        Amount = y.Amount,
                        AuthName = y.AuthName,
                        AuthUser = y.AuthUser,
                        Descript = y.Descript,
                        DiscountId = y.DiscountId,
                        DiscountType = y.DiscountType,
                        ExtraInfo = y.ExtraInfo,
                        Percent = y.Percent,
                        SaleDetailId = y.SaleDetailId,
                        SaleLineDiscountId = y.SaleLineDiscountId,
                    }).ToList(),
                }).ToList(),
                SaleDiscounts = param.SaleDiscount.Select(x => new SaleDiscount()
                {
                    Amount = x.Amount,
                    AuthName = x.AuthName,
                    AuthUser = x.AuthUser,
                    Descript = x.Descript,
                    DiscountId = x.DiscountId,
                    DiscountType = x.DiscountType,
                    ExtraInfo = x.ExtraInfo,
                    Percent = x.Percent,
                    SaleDiscountId = x.SaleDiscountId,
                    SaleId = x.SaleId
                }).ToList()
            };
            UnitOfWork.Sale.Add(sale);
            return sale;
        }
    }
}
