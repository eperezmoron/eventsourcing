﻿using BillingApi.DataAccess.Entities;
using BillingApi.DataAccess.UnitOfWork;
using BillingApi.Dto.DataTransfer;
using BillingApi.Dto.Param;
using Generic.Data.Repositories.Impl;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Generic.DataTransfer.Enums;

namespace BillingApi.DataAccess.Repositories.Impl
{
    public class RepoInvoice : RepoBase<IBillingUnitOfWork>, IRepoInvoice
    {

        public RepoInvoice(IBillingUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public DosageDto GetDosageById(long autorizationNumber)
        {
            return (from a in UnitOfWork.Dosage
                    where a.AutorizationNumber == autorizationNumber
                    select new DosageDto()
                    {
                        State = a.State,
                        DeadlineDate = a.DeadlineDate,
                        AutorizationNumber = a.AutorizationNumber,
                        CreateDate = a.CreateDate,
                        DosageKey = a.DosageKey,
                        EconomicActivityId = a.EconomicActivityId,
                        EconomicActivityLeyend = a.EconomicActivityLeyend,
                        EditDate = a.EditDate,
                        GroupKey = a.GroupKey,
                        InitialInvoiceNumber = a.InitialInvoiceNumber,
                        InvoiceNumber = a.InvoiceNumber,
                        Management = a.Management,
                        Name = a.Name
                    }).FirstOrDefault();
        }

        public string GetEconomicActivityLeyend(short economicActivityId)
        {
            return (from e in UnitOfWork.EconomicActivityType
                    where e.EconomicActivityId == economicActivityId
                    select e.Leyend).FirstOrDefault();
        }

        public DosageDto GetEnableDosage(GetEnableDosageParam param)
        {
            return (from t in UnitOfWork.FiscalDocumentType
                    join a in UnitOfWork.Dosage
                      on t.GroupKey equals a.GroupKey
                    where a.DeadlineDate > param.EmissionDate
                    && a.State == StateEnum.Enabled
                    select new DosageDto()
                    {
                        State = a.State,
                        DeadlineDate = a.DeadlineDate,
                        AutorizationNumber = a.AutorizationNumber,
                        CreateDate = a.CreateDate,
                        DosageKey = a.DosageKey,
                        EconomicActivityId = a.EconomicActivityId,
                        EconomicActivityLeyend = a.EconomicActivityLeyend,
                        EditDate = a.EditDate,
                        GroupKey = a.GroupKey,
                        InitialInvoiceNumber = a.InitialInvoiceNumber,
                        InvoiceNumber = a.InvoiceNumber,
                        Management = a.Management,
                        Name = a.Name
                    }).FirstOrDefault();
        }

        public void IncInvoiceDosageNumber(IncInvoiceDosageNumberParam param)
        {
            Dosage dosage = (from a in UnitOfWork.Dosage
                             where a.AutorizationNumber == param.AutorizationNumber
                             select a).FirstOrDefault();
            if (dosage != null)
            {
                dosage.InvoiceNumber = param.InvoiceNumber;
                dosage.EditDate = DateTime.Now;
            }
        }
    }
}
