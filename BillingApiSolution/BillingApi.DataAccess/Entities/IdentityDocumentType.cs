﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.DataAccess.Entities
{
    public partial class IdentityDocumentType
    {
        public short IdentityDocumentTypeId { get; set; }
        public string Name { get; set; }
        public string KeySin { get; set; }
        public string Descript { get; set; }
        public string Title { get; set; }
        public short State { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }

        public virtual ICollection<FiscalDocument> FiscalDocuments { get; set; }
        public virtual ICollection<Sale> Sales { get; set; }
        public IdentityDocumentType()
        {
            this.FiscalDocuments = new HashSet<FiscalDocument>();
            this.Sales = new HashSet<Sale>();
        }
    }
}
