﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Enum
{
    public static class IdentityDocumentTypeEnum
    {
        public static short CarnetIdentidad = 1;
        public static short CarnetRecidente = 2;
        public static short Pasaporte = 3;
        public static short Otro = 4;
        public static short Nit = 5;
    }
}
