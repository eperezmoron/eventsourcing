﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Enum
{
    public static class SaleStateEnum
    {
        public static short Pendiente { get; set; }
        public static short Facturada { get; set; }
        public static short Anulada { get; set; }
    }
}
