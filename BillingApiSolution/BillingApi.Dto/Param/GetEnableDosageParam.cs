﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Param
{
    public class GetEnableDosageParam
    {
        public short FiscalDocumentTypeId { get; set; }
        public short EconomicActivityTypeId { get; set; }
        public DateTime EmissionDate { get; set; }
    }
}
