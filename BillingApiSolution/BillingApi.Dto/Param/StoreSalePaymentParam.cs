﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BillingApi.Dto.Param
{
    public partial class StoreSalePaymentParam
    {
        public string PaymentType { get; set; }
        public decimal Amount { get; set; }
        public decimal Change { get; set; }
        public string CardNumber { get; set; }
        public string ClientCode { get; set; }
        public string ConfirmationCode { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string AuthCode { get; set; }
        public string ExtraInfo { get; set; }
    }
}
