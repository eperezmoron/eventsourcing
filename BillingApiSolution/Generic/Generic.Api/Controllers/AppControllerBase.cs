﻿using Generic.Domain.AppServices;
using Generic.Domain.Dtos;
using Generic.Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Generic.Api.Controllers
{
    public class AppControllerBase : ControllerBase, IDisposable
    {
        protected readonly IApplicationService _mainService;
        protected readonly ILogger _log = Log.ForContext<AppControllerBase>();

        public AppControllerBase(IApplicationService mainService)
        {
            this._mainService = mainService;
        }

        protected Task<ApiResponse<TData>> HandleCommandResponse<TData>(object param)
            where TData : class
        {
            return ProcessReponseAsync<TData>(RequestHandler.HandleCommand(param, this._mainService.Handle, this._log));
        }

        protected async Task<ApiResponse<TData>> ProcessReponseAsync<TData>(Task<object> data, string message = null)
            where TData : class
        {
            return new ApiResponse<TData>()
            {
                Codigo = (short)ApiResponseEnum.Success,
                Data = (await data) as TData,
                Mensaje = message
            };
        }

        protected ApiResponse<TData> ProcessReponse<TData>(TData data, string message = null)
        {
            return new ApiResponse<TData>()
            {
                Codigo = (short)ApiResponseEnum.Success,
                Data = data,
                Mensaje = message                 
            };
        }

        public virtual void Dispose()
        {
        }
    }
}
