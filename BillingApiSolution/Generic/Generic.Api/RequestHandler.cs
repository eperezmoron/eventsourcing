﻿using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Api
{
    public static class RequestHandler
    {
        public static async Task<TResponse> HandleCommand<TParam, TResponse>(
            TParam request, Func<TParam, Task<TResponse>> handler, ILogger log)
        {           
            try
            {
                log.Error("Handling HTTP request of type {type}", typeof(TParam).Name);
                TResponse response = await handler(request);
                return response;
            }
            catch (Exception e)
            {
                log.Error(e, "Error handling the command");
                throw e;
            }
        }
    }
}
