﻿using Billing.Domain.AppService.Dosage;
using Billing.Domain.Commands.Core;
using Billing.Domain.Dtos.Core;
using Billing.Domain.Repository.Core;
using Generic.Infraestructure.AppServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.AppServices.Dosage
{
    public class ServiceDosageFiscalDocumentCreDeb : ServiceBase<IRepoSale>, IServiceDosageFiscalDocumentCreDeb
    {
        public ServiceDosageFiscalDocumentCreDeb(IRepoSale mainRepo) : base(mainRepo)
        {
        }

        public void ProcessFiscalDocumentCreDeb(StoreSaleParam param, SaleDto saleDto)
        {
            
        }
    }
}
