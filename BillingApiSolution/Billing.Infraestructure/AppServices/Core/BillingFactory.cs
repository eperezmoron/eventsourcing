﻿using Billing.Domain.AppService.Core;
using Billing.Domain.AppService.Dosage;
using Billing.Domain.Enums.Core;
using Generic.Infraestructure.AppServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.AppServices.Core
{
    public partial class BillingFactory : ServiceBase, IBillingFactory
    {
        private readonly IServiceDosageInvoice _serviceDosageInvoice;
        private readonly IServiceDosageFiscalDocumentCreDeb _serviceDosageFiscalDocumentCreDeb;

        public BillingFactory(
            IServiceDosageInvoice serviceDosageInvoice,
            IServiceDosageFiscalDocumentCreDeb serviceDosageFiscalDocumentCreDeb)
        {
            this._serviceDosageInvoice = serviceDosageInvoice;
            this._serviceDosageFiscalDocumentCreDeb = serviceDosageFiscalDocumentCreDeb;
        }

        public override void Dispose()
        {
            this._serviceDosageInvoice?.Dispose();
            this._serviceDosageFiscalDocumentCreDeb?.Dispose();
        }

        public IServiceInvoice GetBillingService(short invoiceType)
        {
            IServiceInvoice service = null;
            if (invoiceType == InvoiceTypeEnum.Dosage)
            {
                service = this._serviceDosageInvoice;
            }
            else if (invoiceType == InvoiceTypeEnum.SFE)
            {
                
            }
            return service;
        }

        public IServiceFiscalDocumentCreDeb GetFiscalDocumentCreDeService(short invoiceType)
        {
            IServiceFiscalDocumentCreDeb service = null;
            if (invoiceType == InvoiceTypeEnum.Dosage)
            {
                service = this._serviceDosageFiscalDocumentCreDeb;
            }
            else if (invoiceType == InvoiceTypeEnum.SFE)
            {

            }
            return service;
        }
    }
}
