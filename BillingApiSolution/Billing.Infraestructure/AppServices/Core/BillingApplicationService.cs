﻿using Billing.Domain.AppService.Core;
using Billing.Domain.Commands.Core;
using Billing.Domain.Dtos.Core;
using Generic.Domain.AppServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Billing.Infraestructure.AppServices.Core
{
    public class BillingApplicationService : IApplicationService
    {
        private readonly IServiceBilling _serviceBilling;

        public BillingApplicationService(IServiceBilling serviceBilling)
        {
            this._serviceBilling = serviceBilling;
        }

        public Task<object> Handle(object command)
        {
            switch (command)
            {
                case StoreSaleParam e:
                    return HandleStoreSale(e);
            }
            return Task.FromResult(default(object));
        }

        private async Task<object> HandleStoreSale(StoreSaleParam c)
        {
            return _serviceBilling.Store(c);
        }
    }
}
