﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.Persistence.Entities.Core
{
    public partial class EconomicActivityType
    {
        public short EconomicActivityId { get; set; }
        public string Name { get; set; }
        public string KeySin { get; set; }
        public string Descript { get; set; }
        public string Leyend { get; set; }
        public short State { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }

        public virtual ICollection<FiscalDocument> FiscalDocuments { get; set; }
        public virtual ICollection<Dosage> Dosages { get; set; }
        public virtual ICollection<Sale> Sales { get; set; }

        public EconomicActivityType()
        {
            this.FiscalDocuments = new HashSet<FiscalDocument>();
            this.Dosages = new HashSet<Dosage>();
            this.Sales = new HashSet<Sale>();
        }
    }
}
