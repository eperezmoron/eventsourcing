﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.Persistence.Entities.Core
{
    public class SaleState
    {
        public short SaleStateId { get; set; }
        public string Name { get; set; }
        public string Descript { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime EditDate { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }

        public SaleState()
        {
            this.Sales = new HashSet<Sale>();
        }
    }
}
