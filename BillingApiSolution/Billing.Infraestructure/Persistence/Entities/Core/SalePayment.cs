﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.Persistence.Entities.Core
{
    public partial class SalePayment
    {
        public long SalePaymentId { get; set; }
        public long SaleId { get; set; }
        public string PaymentType { get; set; }
        public decimal Amount { get; set; }
        public decimal Change { get; set; }
        public string CardNumber { get; set; }
        public string ClientCode { get; set; }
        public string ConfirmationCode { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string AuthCode { get; set; }
        public string ExtraInfo { get; set; }
        public virtual Sale Sale { get; set; }
    }
}
