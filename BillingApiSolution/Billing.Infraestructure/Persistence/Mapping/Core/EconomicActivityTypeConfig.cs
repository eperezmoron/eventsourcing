﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Billing.Infraestructure.Persistence.Entities.Core;

namespace Billing.Infraestructure.Persistence.Mapping.Core
{
   public class EconomicActivityTypeConfig : IEntityTypeConfiguration<EconomicActivityType>
    {
        public void Configure(EntityTypeBuilder<EconomicActivityType> builder)
        {
            builder
                .HasKey(c => c.EconomicActivityId);
            builder
               .Property(c => c.EconomicActivityId)
               .ValueGeneratedNever();

            builder
                .Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired(true);

            builder
                .Property(c => c.KeySin)
                .HasMaxLength(20)
                .IsRequired(true);

            builder
               .Property(c => c.Descript)
               .HasMaxLength(255)
               .IsRequired(false);

            builder
               .Property(c => c.Leyend)
               .HasMaxLength(255)
               .IsRequired(false);

            builder
             .Property(c => c.State)
             .IsRequired(true);
            builder
              .Property(c => c.CreateDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));

            builder
              .Property(c => c.EditDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));


        }

    }
}
