﻿using Billing.Infraestructure.Persistence.Entities.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.Persistence.Mapping.Core
{
    public class SaleDetailTypeConfig : IEntityTypeConfiguration<SaleDetailType>
    {
        public void Configure(EntityTypeBuilder<SaleDetailType> builder)
        {
            builder
                  .HasKey(c => c.SaleDetailTypeId);

            builder
                .Property(c => c.SaleDetailTypeId)
                .ValueGeneratedNever();

            builder
                .Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired(true);

            builder
                .Property(c => c.Descript)
                .HasMaxLength(255)
                .IsRequired(false);

            builder
              .Property(c => c.State)
              .IsRequired(true);

            builder
              .Property(c => c.CreateDate)
              .HasColumnType(string.Format($"datetime"))
              .IsRequired(true);

            builder
              .Property(c => c.EditDate)
              .HasColumnType(string.Format($"datetime"))
              .IsRequired(true);
        }
    }
}
