﻿using Billing.Infraestructure.Persistence.Entities.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.Persistence.Mapping.Core
{
    public class SaleDiscountConfig : IEntityTypeConfiguration<SaleDiscount>
    {
        public void Configure(EntityTypeBuilder<SaleDiscount> builder)
        {
            builder
                  .HasKey(c => c.SaleDiscountId);

            builder
                .Property(c => c.SaleDiscountId)
                .ValueGeneratedOnAdd();

            builder
                .Property(c => c.SaleId)
                .IsRequired(true);

            builder
                .Property(c => c.DiscountId)
                .HasMaxLength(22)
                .IsRequired(true);

            builder
                .Property(c => c.DiscountType)
                .HasMaxLength(50)
                .IsRequired(true);

            builder
                .Property(c => c.Descript)
                .HasMaxLength(255)
                .IsRequired(false);

            builder
                .Property(c => c.Percent)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.Amount)
                .HasColumnType("decimal(14, 2)")
                .IsRequired(true);

            builder
                .Property(c => c.AuthName)
                .HasMaxLength(255)
                .IsRequired(false);

            builder
                .Property(c => c.AuthUser)
                .HasMaxLength(20)
                .IsRequired(false);

            builder
                .Property(c => c.ExtraInfo)
                .IsRequired(true);

            builder
                .HasOne(c => c.Sale)
                .WithMany(c => c.SaleDiscounts)
                .HasForeignKey(c => c.SaleId);

        }
    }
}
