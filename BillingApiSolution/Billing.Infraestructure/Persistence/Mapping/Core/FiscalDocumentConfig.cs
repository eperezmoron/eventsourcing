﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Billing.Infraestructure.Persistence.Entities.Core;

namespace Billing.Infraestructure.Persistence.Mapping.Core
{
    public class FiscalDocumentConfig : IEntityTypeConfiguration<FiscalDocument>
    {
        public void Configure(EntityTypeBuilder<FiscalDocument> builder)
        {
            builder
                .HasKey(c => c.SaleId);

            builder
                .Property(c => c.SaleId)
                .ValueGeneratedNever();
            builder
                .Property(c => c.InvoiceNumber)
                .IsRequired(true);

            builder
                .Property(c => c.EmissionDate)
                .HasColumnType(string.Format($"datetime"))
                .IsRequired(true);

            builder
                .Property(c => c.ControlCode)
                .HasMaxLength(20)
                .IsRequired(false);

            builder
                .Property(c => c.DosageKey)
                .HasMaxLength(200)
                .IsRequired(false);

            builder
                .Property(c => c.AutorizationNumber)
                .IsRequired(false);

            builder
                .Property(c => c.DeadlineEmissionDate)
                .HasColumnType(string.Format($"datetime"))
                .IsRequired(false);

            builder
                .Property(c => c.IdentityDocumentTypeId)
                .IsRequired(false);

            builder
                .Property(c => c.IdentityDocument)
                .HasMaxLength(50)
                .IsRequired(false);

            builder
                .Property(c => c.IdentityComplement)
                .HasMaxLength(5)
                .IsRequired(false);

            builder
                .Property(c => c.FirstClientName)
                .HasMaxLength(100)
                .IsRequired(false);

            builder
                .Property(c => c.EconomicActivityId)
                .IsRequired(true);

            builder
                .Property(c => c.Cuf)
                .HasMaxLength(100)
                .IsRequired(false);
            builder
                .Property(c => c.Cufd)
                .HasMaxLength(100)
                .IsRequired(false);
            builder
                .Property(c => c.Cuis)
                .HasMaxLength(100)
                .IsRequired(false);

            builder
               .Property(c => c.CompanyLeyend)
               .HasMaxLength(400)
               .IsRequired(false);
            builder
               .Property(c => c.CompanyAddress)
               .HasMaxLength(400)
               .IsRequired(false);
            builder
               .Property(c => c.CompanyPhone)
               .HasMaxLength(400)
               .IsRequired(false);
            builder
               .Property(c => c.CompanyNit)               
               .IsRequired(false);
            builder
               .Property(c => c.DocumentFiscalTitle)
               .HasMaxLength(50)
               .IsRequired(false);
            builder
               .Property(c => c.QrLeyend)
               .HasMaxLength(400)
               .IsRequired(false);
            builder
              .Property(c => c.QrBuffer)
              .HasMaxLength(1000)
              .IsRequired(false);

            builder
              .Property(c => c.EconomicActivityLeyend)
              .HasMaxLength(500)
              .IsRequired(false);

            builder
              .Property(c => c.BranchOfficeLeyend)
              .HasMaxLength(500)
              .IsRequired(false);

            builder
              .Property(c => c.FiscalDocumentLeyend)
              .HasMaxLength(500)
              .IsRequired(false);

            builder
              .Property(c => c.InvoiceLeyend)
              .HasMaxLength(500)
              .IsRequired(false);

            builder
              .Property(c => c.InvoiceStateLeyend)
              .HasMaxLength(500)
              .IsRequired(false);
            builder
              .Property(c => c.State)
              .IsRequired(true);
            builder
               .Property(c => c.PostVoidDate)
               .HasColumnType(string.Format($"datetime"))
               .IsRequired(false);
            builder
               .Property(c => c.PostVoidMotiveId)              
               .IsRequired(false);

            builder
                .HasOne(c => c.Dosage)
                .WithMany(c => c.FiscalDocuments)
                .HasForeignKey(c => c.AutorizationNumber)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.EconomicActivity)
                .WithMany(c => c.FiscalDocuments)
                .HasForeignKey(c => c.EconomicActivityId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.FiscalDocumentType)
                .WithMany(c => c.FiscalDocuments)
                .HasForeignKey(c => c.FiscalDocumentTypeId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(c => c.IdentityDocumentType)
                .WithMany(c => c.FiscalDocuments)
                .HasForeignKey(c => c.IdentityDocumentTypeId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
