﻿using Billing.Infraestructure.Persistence.Entities.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.Persistence.Mapping.Core
{
    public class SaleStateConfig : IEntityTypeConfiguration<SaleState>
    {
        public void Configure(EntityTypeBuilder<SaleState> builder)
        {
            builder
                .HasKey(c => c.SaleStateId);

            builder
                .Property(c => c.SaleStateId)
                .ValueGeneratedNever();

            builder
                .Property(c => c.Name)
                .HasMaxLength(100)
                .IsRequired(true);

            builder
               .Property(c => c.Descript)
               .HasMaxLength(255)
               .IsRequired(false);

            builder
              .Property(c => c.CreateDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));

            builder
              .Property(c => c.EditDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));
        }
    }
}
