﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Billing.Infraestructure.Persistence.Entities.Core;

namespace Billing.Infraestructure.Persistence.Mapping.Core
{
   public class FiscalDocumentTypeConfig : IEntityTypeConfiguration<FiscalDocumentType>
    {
        public void Configure(EntityTypeBuilder<FiscalDocumentType> builder)
        {
            builder
               .HasKey(c => c.FiscalDocumentTypeId);
            builder
                .Property(c => c.FiscalDocumentTypeId)
                .ValueGeneratedNever();

            builder
               .Property(c => c.Name)
               .HasMaxLength(100)
               .IsRequired(true);
            builder
            .Property(c => c.KeySin)
            .HasMaxLength(22)
            .IsRequired(false);

            builder
            .Property(c => c.GroupKey)
            .HasMaxLength(22)
            .IsRequired(false);

            builder
            .Property(c => c.Descript)
            .HasMaxLength(255)
            .IsRequired(false);
            builder
            .Property(c => c.Title)
            .HasMaxLength(100)
            .IsRequired(true);
            builder
              .Property(c => c.State)
              .IsRequired(true);
            builder
              .Property(c => c.CreateDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));
            builder
              .Property(c => c.EditDate)
              .IsRequired(true)
              .HasColumnType(string.Format($"datetime"));

        }
    }
}
