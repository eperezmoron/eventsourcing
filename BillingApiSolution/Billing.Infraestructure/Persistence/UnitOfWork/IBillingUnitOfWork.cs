﻿using Billing.Infraestructure.Persistence.Entities.Core;
using Generic.Domain.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Infraestructure.Persistence.UnitOfWork
{
    public interface IBillingUnitOfWork : IUnitOfWorkBase
    {
        DbSet<Dosage> Dosage { get; set; }
        DbSet<EconomicActivityType> EconomicActivityType { get; set; }
        DbSet<FiscalDocument> FiscalDocument { get; set; }
        DbSet<FiscalDocumentCreDe> FiscalDocumentCreDe { get; set; }
        DbSet<FiscalDocumentType> FiscalDocumentType { get; set; }
        DbSet<IdentityDocumentType> IdentityDocumentType { get; set; }
        DbSet<Sale> Sale { get; set; }
        DbSet<SaleDetail> SaleDetail { get; set; }
        DbSet<SaleDetailType> SaleDetailType { get; set; }
        DbSet<SaleDiscount> SaleDiscount { get; set; }
        DbSet<SaleLineDiscount> SaleLineDiscount { get; set; }
        DbSet<SalePayment> SalePayment { get; set; }
        DbSet<SaleState> SaleState { get; set; }
    }
}
