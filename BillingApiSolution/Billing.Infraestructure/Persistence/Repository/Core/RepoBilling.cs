﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Billing.Infraestructure.Persistence.UnitOfWork;
using Billing.Domain.Repository.Core;
using Generic.Infraestructure.Persistence.Repository;

namespace Billing.Infraestructure.Persistence.Repository.Core
{
    public partial class RepoBilling : RepoBase<IBillingUnitOfWork>, IRepoBilling
    {
        public RepoBilling(IBillingUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public string GetFiscalDocumentTitle(short id)
        {
            return (from t in UnitOfWork.FiscalDocumentType
                    where t.FiscalDocumentTypeId == id
                    select t.Title).FirstOrDefault();
        }
    }
}
