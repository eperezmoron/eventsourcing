﻿using Billing.Domain.Dtos.Core;
using Generic.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Repository.Core
{
    public interface IRepoSale : IRepoBase
    {
        SaleDto Store(SaleDto param);
        SaleDto GetById(long param);
    }
}
