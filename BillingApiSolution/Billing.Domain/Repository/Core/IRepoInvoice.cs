﻿using Billing.Domain.Commands.Core;
using Billing.Domain.Dtos.Core;
using Generic.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Repository.Core
{
    public interface IRepoInvoice : IRepoBase
    {
        DosageDto GetEnableDosage(GetEnableDosageParam param);
        DosageDto GetDosageById(long autorizationNumber);
        void IncInvoiceDosageNumber(IncInvoiceDosageNumberParam param);
        string GetEconomicActivityLeyend(short economicActivityId);
    }
}
