﻿using Generic.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Repository.Core
{
    public interface IRepoBilling : IRepoBase
    {
        string GetFiscalDocumentTitle(short id);
    }
}
