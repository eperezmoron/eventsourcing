﻿using Generic.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Repository.Core
{
    public interface IRepoParameter : IRepoBase
    {
        short GetInvoiceType();
        string GetCompanyAddress();

        string GetCompanyNit();
        string GetCompanyLeyend();
        string GetCompanyPhone();
        string GetQrLeyend();
    }
}
