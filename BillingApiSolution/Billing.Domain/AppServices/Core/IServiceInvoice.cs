﻿using Billing.Domain.Dtos.Core;
using Generic.Domain.AppServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.AppService.Core
{
    public interface IServiceInvoice : IServiceBase
    {
        short InvoiceType { get; }
        void GenerateBillingObjects(SaleDto param);
        string GenerateQrBuffer(SaleDto param, FiscalDocumentDto fiscalDocumentDto);
    }
}
