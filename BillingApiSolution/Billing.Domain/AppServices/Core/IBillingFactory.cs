﻿using Generic.Domain.AppServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.AppService.Core
{
    public interface IBillingFactory : IServiceBase
    {
        IServiceInvoice GetBillingService(short invoiceType);
        IServiceFiscalDocumentCreDeb GetFiscalDocumentCreDeService(short invoiceType);
    }
}
