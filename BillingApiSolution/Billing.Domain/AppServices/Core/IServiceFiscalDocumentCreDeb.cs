﻿using Billing.Domain.Commands.Core;
using Billing.Domain.Dtos.Core;
using Generic.Domain.AppServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.AppService.Core
{
    public interface IServiceFiscalDocumentCreDeb : IServiceBase
    {
        void ProcessFiscalDocumentCreDeb(StoreSaleParam param, SaleDto saleDto);
    }
}
