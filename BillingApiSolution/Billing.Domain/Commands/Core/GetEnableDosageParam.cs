﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Commands.Core
{
    public class GetEnableDosageParam
    {
        public short FiscalDocumentTypeId { get; set; }
        public short EconomicActivityTypeId { get; set; }
        public DateTime EmissionDate { get; set; }
    }
}
