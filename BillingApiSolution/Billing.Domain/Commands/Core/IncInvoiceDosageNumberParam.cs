﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Commands.Core
{
    public class IncInvoiceDosageNumberParam
    {
        public long AutorizationNumber { get; set; }
        public long InvoiceNumber { get; set; }
    }
}
