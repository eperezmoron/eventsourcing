﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Enums.Core
{
    public static class SectorDocumentTypeEnum
    {
        public static short FacturaStandard = 1;
        public static short NotaFiscalZonaFranca = 14;
        public static short NotaCreditoDebito = 18;
    }
}
