﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Enums.Core
{
    public static class EmissionTypeEnum
    {
        public static short Online = 1;
        public static short Offline = 2;
    }
}
