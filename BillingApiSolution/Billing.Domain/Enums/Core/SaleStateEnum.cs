﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Billing.Domain.Enums.Core
{
    public static class SaleStateEnum
    {
        public static short Pendiente { get; set; }
        public static short Facturada { get; set; }
        public static short Anulada { get; set; }
    }
}
