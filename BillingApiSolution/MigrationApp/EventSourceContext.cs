﻿using Billing.Infraestructure.Persistence.Entities.Core;
using Billing.Infraestructure.Persistence.Mapping.Core;
using Billing.Infraestructure.Persistence.UnitOfWork;
using Generic.Infraestructure.Persistence.Entities;
using Generic.Infraestructure.Persistence.Mapping;
using Generic.Infraestructure.Persistence.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MigrationApp
{
    public class EventSourceContext : DbContext, IEventSourceUnitOfWork
    {
        public EventSourceContext() : base()
        {
        }

        public virtual DbSet<EventSource> EventSource { get; set; }
       
        public Task<int> SaveChangesAsync()
        {
            return SaveChangesAsync(default(CancellationToken));
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(
                @"Server=.;User ID=user;Password=user;Database=dbBillingEventSource;");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);            
            modelBuilder.ApplyConfiguration(new EventSourceConfig());
        }
    }
}
