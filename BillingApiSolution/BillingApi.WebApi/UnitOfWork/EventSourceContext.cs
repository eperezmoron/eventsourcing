﻿using Billing.Infraestructure.Persistence.Entities.Core;
using Billing.Infraestructure.Persistence.Mapping.Core;
using Billing.Infraestructure.Persistence.UnitOfWork;
using Generic.Infraestructure.Persistence.Entities;
using Generic.Infraestructure.Persistence.Mapping;
using Generic.Infraestructure.Persistence.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BillingApi.WebApi.UnitOfWork
{
    public class EventSourceContext : DbContextUnitOfWork, IEventSourceUnitOfWork
    {
        public EventSourceContext(DbContextOptions<EventSourceContext> options) : base(options)
        {
        }

        public DbSet<EventSource> EventSource { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return SaveChangesAsync(default(CancellationToken));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new EventSourceConfig());
        }
    }
}
