﻿using Billing.Domain.Commands.Core;
using Billing.Domain.Dtos.Core;
using Generic.Api;
using Generic.Api.Controllers;
using Generic.Domain.AppServices;
using Generic.Domain.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace BillingApi.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BillingController : AppControllerBase
    {
        public BillingController(IApplicationService mainService) : base(mainService)
        {
        }

        [Route("store")]
        [HttpPost]
        public Task<ApiResponse<SaleDto>> Store([FromBody] StoreSaleParam param)
        {
            return HandleCommandResponse<SaleDto>(param);
        }
    }
}
