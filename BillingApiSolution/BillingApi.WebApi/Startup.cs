using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Billing.Domain.AppService.Core;
using Billing.Domain.AppService.Dosage;
using Billing.Domain.Repository.Core;
using Billing.Infraestructure.AppServices.Core;
using Billing.Infraestructure.AppServices.Dosage;
using Billing.Infraestructure.Persistence.Repository.Core;
using Billing.Infraestructure.Persistence.UnitOfWork;
using Billing.Lib.Business;
using Billing.Lib.Business.Impl;
using BillingApi.WebApi.UnitOfWork;
using Generic.Api.Filters;
using Generic.Domain.AppServices;
using Generic.Domain.Repository;
using Generic.Infraestructure.Persistence.Repository;
using Generic.Infraestructure.Persistence.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace BillingApi.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => options.Filters.Add(typeof(GenericExcepcionFilter)))
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            Func<IServiceProvider, Context> contextFunc = (serviceProvider) =>
            {
                DbContextOptionsBuilder<Context> builder = new DbContextOptionsBuilder<Context>();
                string connectionString = Configuration.GetConnectionString("AppContext");
                builder.UseSqlServer(connectionString);
                return new Context(builder.Options);
            };
            services.AddScoped<IBillingUnitOfWork, Context>(contextFunc);

            Func<IServiceProvider, EventSourceContext> eventSourceContextFunc = (serviceProvider) =>
            {
                DbContextOptionsBuilder<EventSourceContext> builder = new DbContextOptionsBuilder<EventSourceContext>();
                string connectionString = Configuration.GetConnectionString("EventSourceContext");
                builder.UseSqlServer(connectionString);
                return new EventSourceContext(builder.Options);
            };
            services.AddScoped<IEventSourceUnitOfWork, EventSourceContext>(eventSourceContextFunc);

            services.AddScoped<IServiceDosageFiscalDocumentCreDeb, ServiceDosageFiscalDocumentCreDeb>();
            services.AddScoped<IServiceDosageInvoice, ServiceDosageInvoice>();
            services.AddScoped<IServiceDosageFiscalDocumentCreDeb, ServiceDosageFiscalDocumentCreDeb>();
            services.AddScoped<IServiceDosageInvoice, ServiceDosageInvoice>();
            services.AddScoped<IBillingFactory, BillingFactory>();
            services.AddScoped<IServiceBilling, ServiceBilling>();
            services.AddScoped<IControlCodeGenerator, CodeGenerator7>();
            services.AddScoped<IApplicationService, BillingApplicationService>();

            #region Repositories            
            services.AddScoped<IRepoSale, RepoSale>();
            services.AddScoped<IRepoInvoice, RepoInvoice>();
            services.AddScoped<IRepoBilling, RepoBilling>();
            services.AddScoped<IRepoParameter, RepoParameter>();
            services.AddScoped<IRepoEventSource, RepoEventSource>();
            #endregion

            #region Services           
            //services.AddScoped<IServiceSession, ServiceSession>();
            #endregion
            services.AddControllers();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
