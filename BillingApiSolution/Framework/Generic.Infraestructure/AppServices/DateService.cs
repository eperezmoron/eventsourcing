﻿using Generic.Domain.AppServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Infraestructure.AppServices.Impl
{
    public class DateService : IDateService
    {
        public DateTime Now()
        {
            return DateTime.Now;
        }
    }
}
