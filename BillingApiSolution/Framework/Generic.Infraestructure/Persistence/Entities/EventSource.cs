﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Infraestructure.Persistence.Entities
{
    public class EventSource
    {
        public long EventSourceId { get; set; }
        public string EntityClass { get; set; }
        public string EntityId { get; set; }
        public string EntityHash { get; set; }
        public DateTime CreateDate { get; set; }
        public string Entity { get; set; }
    }
}
