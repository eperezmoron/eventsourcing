﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Generic.Infraestructure.Persistence.Entities;

namespace Generic.Infraestructure.Persistence.Mapping
{
    public class EventSourceConfig : IEntityTypeConfiguration<EventSource>
    {
        public void Configure(EntityTypeBuilder<EventSource> builder)
        {
            builder
                .HasKey(c => c.EventSourceId);
            builder
                .Property(c => c.EventSourceId)
                .ValueGeneratedOnAdd();

            builder
                .Property(c => c.EntityClass)
                .HasMaxLength(1000)
                .IsRequired(true);
            builder
                .Property(c => c.EntityHash)
                .HasMaxLength(1000)
                .IsRequired(true);
            builder
                .Property(c => c.EntityId)
                .HasMaxLength(100)
                .IsRequired(true);
            builder
                .Property(c => c.Entity)                
                .IsRequired(true);
            builder
                .Property(c => c.CreateDate)
                .HasColumnType(string.Format($"datetime"))
                .IsRequired(true);
        }
    }
}
