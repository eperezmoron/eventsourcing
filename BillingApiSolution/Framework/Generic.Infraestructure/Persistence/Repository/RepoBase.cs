﻿using Generic.Domain.Repository;
using Generic.Domain.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Infraestructure.Persistence.Repository
{
    public abstract class RepoBase<TUnitOfWorkBase> : IRepoBase
        where TUnitOfWorkBase : IUnitOfWorkBase
    {
        public TUnitOfWorkBase UnitOfWork { get; }

        public IUnitOfWorkBase UnitOfWorkBase => UnitOfWork;

        public RepoBase(
            TUnitOfWorkBase unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public void Dispose()
        {
            this.UnitOfWork?.Dispose();
        }
    }
}
