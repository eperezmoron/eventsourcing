﻿using Generic.Domain;
using Generic.Domain.Commands;
using Generic.Domain.Dtos;
using Generic.Domain.Repository;
using Generic.Infraestructure.Persistence.Entities;
using Generic.Infraestructure.Persistence.Repository;
using Generic.Infraestructure.Persistence.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Generic.Infraestructure.Persistence.Repository
{
    public class RepoEventSource : RepoBase<IEventSourceUnitOfWork>, IRepoEventSource
    {
        public RepoEventSource(IEventSourceUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<long> Store(EventSourceParam eventSourceDto)
        {
            string json = JsonSerializer.Serialize(eventSourceDto.Entity);
            EventSource eventSource = new EventSource()
            {
                EntityHash = Utils.SHA256Encrypt(json),
                EntityClass = eventSourceDto.Entity.GetType().FullName,
                EntityId = eventSourceDto.EntityId,
                Entity = json,
                CreateDate = DateTime.Now
            };
            UnitOfWork.EventSource.Add(eventSource);
            await UnitOfWork.SaveChangesAsync();
            return eventSource.EventSourceId;
        }
    }
}
