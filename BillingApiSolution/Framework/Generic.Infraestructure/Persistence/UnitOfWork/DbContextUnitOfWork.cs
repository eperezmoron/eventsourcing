﻿using Generic.Domain.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Generic.Infraestructure.Persistence.UnitOfWork
{
    public class DbContextUnitOfWork : DbContext, IUnitOfWorkBase
    {
        public DbContextUnitOfWork(DbContextOptions options) : base(options)
        {
        }

        public Task<int> SaveChangesAsync()
        {
            return this.SaveChangesAsync(default(CancellationToken));
        }
    }
}
