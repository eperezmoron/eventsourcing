﻿using Generic.Domain.UnitOfWork;
using Generic.Infraestructure.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Infraestructure.Persistence.UnitOfWork
{
    public interface IEventSourceUnitOfWork : IUnitOfWorkBase
    {
        DbSet<EventSource> EventSource { get; set; }
    }
}
