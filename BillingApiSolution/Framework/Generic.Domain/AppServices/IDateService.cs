﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Domain.AppServices
{
    public interface IDateService
    {
        DateTime Now();
    }
}
