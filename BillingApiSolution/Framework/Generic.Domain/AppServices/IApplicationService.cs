﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Domain.AppServices
{
    public interface IApplicationService
    {
        Task<object> Handle(object command);
    }
}
