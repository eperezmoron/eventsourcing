﻿using Generic.Domain.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Domain.Repository
{
    public interface IRepoBase : IDisposable
    {
        IUnitOfWorkBase UnitOfWorkBase { get; }
    }
}
