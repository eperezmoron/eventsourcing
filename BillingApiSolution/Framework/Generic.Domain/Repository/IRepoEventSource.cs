﻿using Generic.Domain.Commands;
using Generic.Domain.Dtos;
using Generic.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Domain.Repository
{
    public interface IRepoEventSource : IRepoBase
    {
        Task<long> Store(EventSourceParam eventSourceDto);
    }
}
