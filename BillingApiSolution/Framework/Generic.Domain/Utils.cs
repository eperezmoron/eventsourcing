﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;

namespace Generic.Domain
{
    public static class Utils
    {
        public const string DateTimeYYYYMMDDHHMMSSFormat = "yyyy-MM-dd'T'HH:mm:ss";
        public const string DateTimeYYYYMMDDFormat = "yyyy-MM-dd";
        public const string DateTimeDDMMYYYYFormat = "dd/MM/yyyy";
        public const string DateTimeDDMMYYYYHHMMSSFormat = "dd/MM/yyyy HH:mm:ss";

        public const string DecimalPointDecimalFormat = "#######0.00";
        private static Random _random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[_random.Next(s.Length)]).ToArray());
        }

        private static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = System.Security.Cryptography.SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static string GenerarHash(params object[] obj)
        {
            return MD5(Convert.ToBase64String(ObjectToByteArray(obj)));
        }

        public static string SHA1(string text)
        {
            var result = default(string);

            using (var algo = new SHA1Managed())
            {
                result = GenerateHashString(algo, text);
            }

            return result;
        }

        public static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        public static string SHA256(string text)
        {
            var result = default(string);

            using (var algo = new SHA256Managed())
            {
                result = GenerateHashString(algo, text);
            }

            return result;
        }

        public static string SHA256(byte[] content)
        {
            var result = default(string);

            using (var algo = new SHA256Managed())
            {
                result = GenerateHashByteArray(algo, content);
            }

            return result;
        }


        public static string SHA384(string text)
        {
            var result = default(string);

            using (var algo = new SHA384Managed())
            {
                result = GenerateHashString(algo, text);
            }

            return result;
        }

        public static string SHA512(string text)
        {
            var result = default(string);

            using (var algo = new SHA512Managed())
            {
                result = GenerateHashString(algo, text);
            }

            return result;
        }

        public static string MD5(string text)
        {
            var result = default(string);

            using (var algo = new MD5CryptoServiceProvider())
            {
                result = GenerateHashString(algo, text);
            }

            return result;
        }

        public static string MD5(byte[] content)
        {
            var result = default(string);

            using (var algo = new MD5CryptoServiceProvider())
            {
                result = GenerateHashByteArray(algo, content);
            }

            return result;
        }

        public static string SHA256Encrypt(string input)
        {
            SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();

            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashedBytes = provider.ComputeHash(inputBytes);

            StringBuilder output = new StringBuilder();

            for (int i = 0; i < hashedBytes.Length; i++)
                output.Append(hashedBytes[i].ToString("x2").ToLower());

            return output.ToString();
        }

        public static string SHA256Encrypt(byte[] input)
        {
            SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();

            byte[] hashedBytes = provider.ComputeHash(input);

            StringBuilder output = new StringBuilder();

            for (int i = 0; i < hashedBytes.Length; i++)
                output.Append(hashedBytes[i].ToString("x2").ToLower());

            return output.ToString();
        }

        private static string GenerateHashString(HashAlgorithm algoritmo, string text)
        {
            // Compute hash from text parameter
            algoritmo.ComputeHash(Encoding.UTF8.GetBytes(text));

            // Get has value in array of bytes
            var result = algoritmo.Hash;

            // Return as hexadecimal string
            return string.Join(
                string.Empty,
                result.Select(x => x.ToString("x2")));
        }

        private static string GenerateHashByteArray(HashAlgorithm algoritmo, byte[] content)
        {
            // Compute hash from text parameter
            algoritmo.ComputeHash(content);

            // Get has value in array of bytes
            var result = algoritmo.Hash;

            // Return as hexadecimal string
            return string.Join(
                string.Empty,
                result.Select(x => x.ToString("x2")));
        }
        #region Exception

        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem,
            Func<TSource, bool> canContinue)
        {
            for (var current = source; canContinue(current); current = nextItem(current))
            {
                yield return current;
            }
        }

        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem)
            where TSource : class
        {
            return FromHierarchy(source, nextItem, s => s != null);
        }

        public static string GetAllMessages(this Exception exception)
        {
            var messages = exception.FromHierarchy(ex => ex.InnerException)
                .Select(ex => ex.Message);
            return string.Join(Environment.NewLine, messages);
        }
        #endregion

        #region String to Type

        public static string ObjectToString(object source)
        {
            return source?.ToString();
        }

        public static string ObjectToStringTrim(object source)
        {
            return source?.ToString()?.Trim();
        }

        public static int ObjectToInt(object source, int defaultIfNullOrEmpty = 0)
        {
            return StringToType(ObjectToString(source), defaultIfNullOrEmpty);
        }

        public static decimal ObjectToDecimal(object source, decimal defaultIfNullOrEmpty = 0)
        {
            return StringToType(ObjectToString(source), defaultIfNullOrEmpty);
        }

        public static DateTime ObjectToDateTime(object source, string format = "yyyy-MM-ddTHH:mm:ss.fff")
        {
            return StringToType(ObjectToString(source), format);
        }

        public static short? StringToTypeOrNull(string source, short? defaultIfNullOrEmpty = 0)
        {
            short response = 0;
            if (!short.TryParse(source?.Trim(), out response))
            {
                return defaultIfNullOrEmpty;
            }
            return response;
        }

        public static int? StringToTypeOrNull(string source, int? defaultIfNullOrEmpty = 0)
        {
            int response = 0;
            if (!int.TryParse(source?.Trim(), out response))
            {
                return defaultIfNullOrEmpty;
            }
            return response;
        }

        public static long? StringToTypeOrNull(string source, long? defaultIfNullOrEmpty = 0)
        {
            long response = 0;
            if (!long.TryParse(source?.Trim(), out response))
            {
                return defaultIfNullOrEmpty;
            }
            return response;
        }

        public static int StringToType(string source, int defaultIfNullOrEmpty = 0)
        {
            int response = defaultIfNullOrEmpty;
            if (!int.TryParse(source?.Trim(), out response))
            {
                response = defaultIfNullOrEmpty;
            }
            return response;
        }

        public static decimal? StringToTypeOrNull(string source, decimal? defaultIfNullOrEmpty = 0)
        {
            decimal response = 0;
            if (!decimal.TryParse(source?.Trim(), out response))
            {
                return defaultIfNullOrEmpty;
            }
            return response;
        }

        public static decimal StringToType(string source, decimal defaultIfNullOrEmpty = 0)
        {
            decimal response = defaultIfNullOrEmpty;
            if (!decimal.TryParse(source?.Trim(), out response))
            {
                response = defaultIfNullOrEmpty;
            }
            return response;
        }

        public static DateTime? StringToType(string source, DateTime? @default, string format = "yyyy-MM-ddTHH:mm:ss.fff")
        {
            if (DateTime.TryParseExact(source?.Trim(), format, CultureInfo.CurrentCulture.DateTimeFormat, DateTimeStyles.None, out DateTime response))
            {
                return response;
            }
            return @default;
        }

        public static DateTime StringToType(string source, string format = "yyyy-MM-ddTHH:mm:ss.fff")
        {
            return StringToType(source, DateTime.Now, format);
        }

        public static DateTime StringToType(string source, DateTime @default, string format = "yyyy-MM-ddTHH:mm:ss.fff")
        {
            DateTime response = @default;
            if (!DateTime.TryParseExact(source?.Trim(), format, CultureInfo.CurrentCulture.DateTimeFormat, DateTimeStyles.None, out response))
            {
                response = @default;
            }
            return response;
        }
        #endregion 
        public static object GetOrInsertToLock<TKey>(
            this IDictionary<TKey, object> dictionary,
            TKey key)
        {
            if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }
            else
            {
                object value = new object();
                dictionary[key] = value;
                return value;
            }
        }

        public static TValue GetOrInsert<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key,
            TValue newValue)
        {
            if (dictionary.ContainsKey(key))
            {
                newValue = dictionary[key];
            }
            else
            {
                dictionary[key] = newValue;
            }
            return newValue;
        }

        public static IEnumerable<List<T>> SplitList<T>(List<T> source, int take)
        {
            for (int i = 0; i < source.Count; i += take)
            {
                yield return source.GetRange(i, Math.Min(take, source.Count - i));
            }
        }
    }
}
