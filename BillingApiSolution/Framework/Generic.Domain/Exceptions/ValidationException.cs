﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Generic.Domain.Exceptions
{
    public class ValidationException : Exception
    {
        public List<string> Errores { get; set; }

        public ValidationException(string message, params string[] errors) : base(message)
        {
            this.Errores = errors.ToList();
        }
    }
}
