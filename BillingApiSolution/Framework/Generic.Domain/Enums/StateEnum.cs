﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Domain.Enums
{
    public static class StateEnum
    {
        public const short Disabled = 0;
        public const short Enabled = 1;
        public const short Deleted = 2;
        public const short PostVoid = 3;
    }
}
