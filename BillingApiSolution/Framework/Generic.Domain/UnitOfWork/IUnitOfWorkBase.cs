﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Domain.UnitOfWork
{
    public interface IUnitOfWorkBase : IDisposable
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
