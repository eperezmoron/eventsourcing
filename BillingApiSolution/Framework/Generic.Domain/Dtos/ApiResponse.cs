﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Domain.Dtos
{
    public class ApiResponse<TData>
    {
        public short Codigo { get; set; }
        public TData Data { get; set; }
        public string Mensaje { get; set; }
        public List<string> Errores { get; set; }
    }
}
