﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generic.Domain.Aggregate.Impl
{
    public abstract class AggregateRoot<TId> : IInternalEventHandler
    {
        public TId Id { get; protected set; }

        protected abstract void When(object @event);

        private readonly List<object> _changes;

        protected AggregateRoot() => this._changes = new List<object>();

        protected void Apply(object @event)
        {
            When(@event);
            ValidateStatus();
            this._changes.Add(@event);
        }

        public IEnumerable<object> GetChanges() => this._changes.AsEnumerable();

        public void ClearChanges() => this._changes.Clear();

        protected abstract void ValidateStatus();

        protected void ApplyToEntity(IInternalEventHandler entity, object @event)
            => entity?.Handle(@event);

        void IInternalEventHandler.Handle(object @event) => When(@event);
    }
}
