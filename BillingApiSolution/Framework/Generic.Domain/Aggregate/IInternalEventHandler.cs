﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Domain.Aggregate
{
    public interface IInternalEventHandler
    {
        void Handle(object @event);
    }
}
