﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Generic.Domain.Commands
{
    public class EventSourceParam
    {
        public string EntityId { get; set; }
        public object Entity { get; set; }
    }
}
