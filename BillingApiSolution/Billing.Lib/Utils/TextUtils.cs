﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Billing.Lib.Utils
{
    public class TextUtils
    {
        #region Literal
        private static string[] Unidad = new string[9]
    {
      "un ",
      "dos ",
      "tres ",
      "cuatro ",
      "cinco ",
      "seis ",
      "siete ",
      "ocho ",
      "nueve "
    };
        private static string[] Quinces = new string[5]
        {
      "once ",
      "doce ",
      "trece ",
      "catorce ",
      "quince "
        };
        private static string[] Decena = new string[9]
        {
      "dieci",
      "veinti",
      "treinta ",
      "cuarenta ",
      "cincuenta ",
      "sesenta ",
      "setenta ",
      "ochenta ",
      "noventa "
        };
        private static string[] Centena = new string[9]
        {
      "cien ",
      "doscientos ",
      "trescientos ",
      "cuatrocientos ",
      "quinientos ",
      "seiscientos ",
      "setecientos ",
      "ochocientos ",
      "novecientos "
        };

        public static string Literal(double n, bool centavosTambien)
        {
            StringBuilder Cadena = new StringBuilder();
            double num1 = Math.Round(Math.Abs(n), 2);
            if (num1 > 999999999.99)
            {
                return "El valor es mayor que 999999999.99 - No puedo procesar";
            }

            long int64 = Convert.ToInt64(num1 * 100.0);
            int num2 = (int)(int64 / 10000000000L);
            long num3 = int64 - (long)num2 * 10000000000L;
            int num4 = (int)(num3 / 1000000000L);
            long num5 = num3 - (long)(num4 * 1000000000);
            int num6 = (int)(num5 / 100000000L);
            long num7 = num5 - (long)(num6 * 100000000);
            int num8 = (int)(num7 / 10000000L);
            long num9 = num7 - (long)(num8 * 10000000);
            int num10 = (int)(num9 / 1000000L);
            long num11 = num9 - (long)(num10 * 1000000);
            int num12 = (int)(num11 / 100000L);
            long num13 = num11 - (long)(num12 * 100000);
            int num14 = (int)(num13 / 10000L);
            long num15 = num13 - (long)(num14 * 10000);
            int num16 = (int)(num15 / 1000L);
            long num17 = num15 - (long)(num16 * 1000);
            int num18 = (int)(num17 / 100L);
            long num19 = num17 - (long)(num18 * 100);
            int num20 = (int)(num19 / 10L);
            int num21 = (int)(num19 - (long)(num20 * 10));
            int num22 = 1;
            if (num2 != 0 || num4 != 0 || num6 != 0)
            {
                int CE = num2;
                int DE = num4;
                int UN = num6;
                Miles(Cadena, CE, DE, UN);
                if (num2 == 0 && num4 == 0 && num6 == 1)
                {
                    Cadena.Append("millón ");
                }
                else
                {
                    Cadena.Append("millones ");
                }
            }
            if (num8 != 0 || num10 != 0 || num12 != 0)
            {
                int CE = num8;
                int DE = num10;
                int UN = num12;
                Miles(Cadena, CE, DE, UN);
                Cadena.Append("mil ");
            }
            if (num14 != 0 || num16 != 0 || num18 != 0)
            {
                int CE = num14;
                int DE = num16;
                int UN = num18;
                Miles(Cadena, CE, DE, UN);
                if (num18 == 1 && num16 != 1)
                {
                    Cadena.Remove(Cadena.Length - 1, 1).Append("o ");
                }
            }
            if (num20 != 0 || num21 != 0)
            {
                if (centavosTambien)
                {
                    if (num22 == 1)
                    {
                        Cadena.Append("con ");
                    }

                    int CE = 0;
                    int DE = num20;
                    int UN = num21;
                    Miles(Cadena, CE, DE, UN);
                    if (num20 == 0 && num21 == 1)
                    {
                        Cadena.Append("centavo.");
                    }
                    else
                    {
                        Cadena.Append("centavos.");
                    }
                }
                else
                {
                    Cadena.Append(num20.ToString() + "" + (object)num21 + "/100 Bs");
                }
            }
            else if (centavosTambien)
            {
                Cadena.Append("Bs");
            }
            else
            {
                Cadena.Append("00/100 Bs");
            }

            return Cadena.ToString();
        }

        private static void Miles(StringBuilder Cadena, int CE, int DE, int UN)
        {
            if (CE != 0)
            {
                Cadena.Append(Centena[CE - 1]);
                if ((DE != 0 || UN != 0) && CE == 1)
                {
                    Cadena.Remove(Cadena.Length - 1, 1).Append("to ");
                }
            }
            if (DE != 0)
            {
                if (DE == 2)
                {
                    if (UN == 0)
                    {
                        Cadena.Append("veinte ");
                        return;
                    }
                    Cadena.Append(Decena[1]);
                }
                if (DE == 1)
                {
                    if (UN == 0)
                    {
                        Cadena.Append("diez ");
                        return;
                    }
                    if (UN > 0 && UN < 6)
                    {
                        Cadena.Append(Quinces[UN - 1]);
                        return;
                    }
                    Cadena.Append(Decena[DE - 1]);
                }
                if (DE > 2)
                {
                    Cadena.Append(Decena[DE - 1]);
                }

                if (UN == 0)
                {
                    return;
                }

                if (DE > 2)
                {
                    Cadena.Append("y ");
                }
            }
            if (UN == 0)
            {
                return;
            }

            Cadena.Append(Unidad[UN - 1]);
        }

        public static string RomperEnLineas(string linea, int n)
        {
            string str = "";
            string[] strArray = linea.Split(' ');
            int num = 0;
            for (int index = 0; index < strArray.Length; ++index)
            {
                if (num + strArray[index].Length >= n)
                {
                    str += "\n";
                    num = 0;
                }
                str = str + strArray[index] + " ";
                num += strArray[index].Length + 1;
            }
            return str;
        }
        #endregion

        public static string SINFormat(string hexstr)
        {
            string str = hexstr.Substring(0, 2).ToUpper();
            int num = hexstr.Length / 2;
            for (int index = 1; index < num; ++index)
            {
                str = str + "-" + hexstr.Substring(index * 2, 2).ToUpper();
            }
            return str;
        }

        public static long ToInt(string source)
        {
            return long.Parse(Regex.Replace(source, "[^.0-9]", ""));
        }
    }
}
