﻿using Billing.Lib.DataTransfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing.Lib.Business
{
    public interface IControlCodeGenerator
    {
        string GenerateCode(GenerateCodeParams input, bool logs);
    }
}
