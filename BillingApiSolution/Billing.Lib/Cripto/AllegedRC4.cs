﻿using System.Globalization;
using System.Text;

namespace Billing.Lib.Cripto
{
    public class AllegedRC4
    {
        private static int Ord(char ch)
        {
            return (int)Encoding.GetEncoding(1252).GetBytes(((int)ch).ToString() + "")[0];
        }

        private static char Chr(int i)
        {
            return Encoding.GetEncoding(1252).GetString(new byte[1]
            {
        (byte) i
            })[0];
        }

        private static string Pack(string packtype, string datastring)
        {
            int length = datastring.Length;
            byte[] bytes = new byte[length / 2 + length % 2];
            char[] chArray = new char[2];
            int num;
            for (int index = num = 0; index < length; index += 2)
            {
                chArray[0] = datastring[index];
                chArray[1] = length - index != 1 ? datastring[index + 1] : '0';
                string s = new string(chArray, 0, 2);
                try
                {
                    bytes[num++] = byte.Parse(s, NumberStyles.HexNumber);
                }
                catch
                {
                }
            }
            return Encoding.GetEncoding(1252).GetString(bytes);
        }

        public static string Bin2hex(string bindata)
        {
            byte[] bytes = Encoding.GetEncoding(1252).GetBytes(bindata);
            string str = "";
            for (int index = 0; index < bytes.Length; ++index)
            {
                str += bytes[index].ToString("x2");
            }

            return str;
        }

        public static string Encrypt(string pwd, string data, bool ispwdHex)
        {
            if (ispwdHex)
            {
                pwd = Pack("H*", pwd);
            }

            int length1 = pwd.Length;
            int length2 = data.Length;
            int[] numArray1 = new int[256];
            int[] numArray2 = new int[256];
            byte[] bytes = new byte[data.Length];
            for (int index = 0; index < 256; ++index)
            {
                numArray1[index] = Ord(pwd[index % length1]);
                numArray2[index] = index;
            }
            int index1;
            for (int index2 = index1 = 0; index1 < 256; ++index1)
            {
                index2 = (index2 + numArray2[index1] + numArray1[index1]) % 256;
                int num = numArray2[index1];
                numArray2[index1] = numArray2[index2];
                numArray2[index2] = num;
            }
            int num1;
            int index3 = num1 = 0;
            int index4 = num1;
            int index5 = num1;
            for (; index3 < length2; ++index3)
            {
                index5 = (index5 + 1) % 256;
                index4 = (index4 + numArray2[index5]) % 256;
                int num2 = numArray2[index5];
                numArray2[index5] = numArray2[index4];
                numArray2[index4] = num2;
                int num3 = numArray2[(numArray2[index5] + numArray2[index4]) % 256];
                bytes[index3] = (byte)(Ord(data[index3]) ^ num3);
            }
            return Encoding.GetEncoding(1252).GetString(bytes);
        }

        public static string Decrypt(string pwd, string data, bool ispwdHex)
        {
            return Encrypt(pwd, data, ispwdHex);
        }
    }
}
